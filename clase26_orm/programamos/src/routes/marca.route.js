const express = require("express");
const router = express.Router();
const db = require("../models");

router.get("/", async (req, res) => {
    const marcas = await db.Marcas.findAll({ include: ["Modelos"] });
    res.json(marcas);
});

router.post("/", async (req, res) => {
    const nombre = req.body.nombre;
    await db.Marcas.create({ nombre });
    res.status(201).json({ message: "Creado" });
});


module.exports = router;