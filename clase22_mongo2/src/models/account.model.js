const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Model = mongoose.model;

const accountSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
    },
    balance: {
        type: Number,
        required: true,
    }
});

module.exports = Model("Cuentas", accountSchema);