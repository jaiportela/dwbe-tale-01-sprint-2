

let promesa1 = new Promise((resolve, reject) => {
    setTimeout(() => { resolve("Resuelto en 5 segundos") }, 5000);
})

promesa1
    .then((message) => {
        console.log(message);
    })