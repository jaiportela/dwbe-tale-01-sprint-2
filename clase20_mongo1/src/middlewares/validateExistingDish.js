const Menu = require("../models/menu")

async function validateExistingDish(req, res, next) {
    try {
        const id = req.params.id;
        await Menu.findById(id);
        next();
    }
    catch (e) {
        res.status(404).json({
            code: "404",
            message: "Plato no encontrado"
        })
    }
}

module.exports = validateExistingDish;