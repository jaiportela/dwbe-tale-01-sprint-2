const mongoose = require("mongoose");

(function () {
    mongoose.connect('', {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        retryWrites: false,
    });
    console.log("Conectado a la base de datos");
}
)()