const fetch = require("node-fetch");
const ciudades = [{ nombre: "Bogota" }, { nombre: "London" }, { nombre: "Barcelona" }, { nombre: "Tokio" }, { nombre: "Montreal" }]


const recibirClima = ({ nombre }) => {
    return fetch(`https://api.openweathermap.org/data/2.5/weather?q=${nombre}&appid=cc94dc5ccd49795403ad2a4288e504e6`)
        .then(response => {
            return response.json()
        })
        .then(resJson => {
            const tempInK = resJson.main.temp;
            return Math.round(tempInK - 273);
        })
        .catch(err => console.log(err));

}
let count = 0;
ciudades.forEach(ciudad => {
    recibirClima(ciudad)
        .then(tempC => {
            ciudad.temperatura = tempC;
            count++;
            return count;
        })
        .then(count => { if (count === ciudades.length) console.log(ciudades); })
        .catch(err => console.log(err))


})