const express = require("express");
require("./db");
const menuRoute = require("./routes/menu.route");


const PORT = 3000;
const app = express();
app.use(express.json());

app.use('/menu', menuRoute);

app.listen(PORT, () => {
    console.log("Listen PORT: " + PORT);
})