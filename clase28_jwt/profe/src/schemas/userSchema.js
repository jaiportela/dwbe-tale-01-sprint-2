const joi = require("joi");

const validateUser = joi.object({
    username: joi
        .string()
        .min(3)
        .max(30)
        .required(),
    email: joi
        .string()
        .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
    password: joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
    repeatPassword: joi.ref('password'),
    isAdmin: joi.boolean()
});

module.exports = validateUser;