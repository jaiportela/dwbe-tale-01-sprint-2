const fetch = require("node-fetch");
const ciudades = [{ nombre: "Bogota" }, { nombre: "London" }, { nombre: "Barcelona" }, { nombre: "Tokio" }, { nombre: "Montreal" }]


const recibirClima = async ({ nombre }) => {
    try {
        const weatherData = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${nombre}&appid=cc94dc5ccd49795403ad2a4288e504e6`)

        const resJson = await weatherData.json()
        const tempInK = resJson.main.temp;
        return Math.round(tempInK - 273);
    } catch (e) {
        console.log(e);
    }
}

const promises = ciudades.map(ciudad => {
    return recibirClima(ciudad);
});

(async () => {
    const temperaturas = await Promise.all(promises);
})()
// ciudades.forEach(async (ciudad) => {
//     try {
//         const tempC = await recibirClima(ciudad)
//         ciudad.temperatura = tempC;
//         count++;
//         if (count === ciudades.length) console.log(ciudades);
//     } catch (e) {
//         console.log(e);
//     }
// })