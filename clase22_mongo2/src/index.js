const express = require("express");
require("./connection/db");
const app = express();

app.use(express.json());

app.use("/cuenta", require("./routes/account.route"));

app.listen(3000, () => {
    console.log("Listen port 3000");
});