const mongoose = require('mongoose');

(async () => {
    await mongoose.connect('mongodb://localhost:27017/jwt', { useNewUrlParser: true, useUnifiedTopology: true })
    console.log('Conectado a la base de datos');
})();