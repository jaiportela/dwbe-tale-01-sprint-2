const Account = require("../models/account.model")

async function validateEmail(req, res, next) {
    const existingAccount = await Account.findOne({ email: req.body.email });
    if (existingAccount) {
        res.status(400).json({
            code: "400",
            message: "El usuario ya existe"
        })
    } else next();
}
async function validateAccount(req, res, next) {
    const existingAccount = await Account.findOne({ email: req.body.email });
    if (existingAccount) next();
    else {
        res.status(404).json({
            code: "404",
            message: "El usuario no existe"
        });
    };
}

module.exports = { validateEmail, validateAccount };