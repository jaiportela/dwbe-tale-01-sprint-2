const { Sequelize, DataTypes } = require('sequelize');

const sequelize = new Sequelize('programamosORM', 'root', '', {
    host: 'localhost',
    dialect: 'mariadb'
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.Televisores = require('./televisor.model')(sequelize, DataTypes);
db.Marcas = require('./marca.model')(sequelize, DataTypes);
db.Modelos = require('./modelo.model')(sequelize, DataTypes);

db.Marcas.hasMany(db.Modelos);
db.Modelos.belongsTo(db.Marcas);

db.Marcas.hasMany(db.Televisores);
db.Televisores.belongsTo(db.Marcas);

db.Modelos.hasMany(db.Televisores);
db.Televisores.belongsTo(db.Modelos);


module.exports = db;
