const express = require('express');
const app = express();
const db = require("./models");
app.use(express.json());

app.use("/televisores", require("./routes/televisor.route"));
app.use("/marcas", require("./routes/marca.route"));
app.use("/modelos", require("./routes/modelo.route"));

db.sequelize.sync({ force: false })
    .then(() => {
        console.log('Conectado a la BD');
        app.listen(3000);
        console.log('Escuchando en el puerto 3000');
    })
    .catch(err => {
        console.log('Error conectado a la bd:' + err);
    });
