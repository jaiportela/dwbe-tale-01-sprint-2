const express = require("express");
const fetch = require("node-fetch");
const app = express()

app.use(express.json());
const api_key = "82164e92";
const url = `http://www.omdbapi.com/?apikey=${api_key}&`

app.get("/peliculas", async (req, res) => {
    const { nombrePelicula } = req.query;
    const resultado = await fetch(url + `t=${nombrePelicula}`);
    const pelicula = await resultado.json();
    res.json({
        titulo: pelicula.Title,
        imagen: pelicula.Poster,
        description: pelicula.Plot,
    });
});

const peliPromesa = async (nombrePelicula) => {
    const resultado = await fetch(url + `t=${nombrePelicula}`);
    const pelicula = await resultado.json();
    return {
        titulo: pelicula.Title,
        imagen: pelicula.Poster,
        description: pelicula.Plot,
    }
}


app.post("/peliculas", async (req, res) => {
    const { peliculas } = req.body;
    const promises = peliculas.map(pelicula => peliPromesa(pelicula));
    const infoPeliculas = await Promise.all(promises);
    res.json(infoPeliculas)
})

app.listen(3000, () => {
    console.log("Escuchando el puerto: 3000");
})