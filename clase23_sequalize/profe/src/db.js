const { Sequelize } = require("sequelize");

const sequelize = new Sequelize('programamos', 'root', null, {
    host: 'localhost',
    dialect: "mariadb"
});

(async function () {
    try {
        await sequelize.authenticate();
        console.log('Conexion a la base de datos exitosa');
    } catch (e) {
        console.log(e);
    }
})()

module.exports = sequelize;