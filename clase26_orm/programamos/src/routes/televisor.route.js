const express = require("express");
const router = express.Router();
const db = require("../models");

router.get("/", async (req, res) => {
    const televisores = await db.Televisores.findAll();
    res.json(televisores);
});

router.post("/", async (req, res) => {
    const { precio, pantalla, smart, cantidad } = req.body;
    await db.Televisores.create({ precio, pantalla, smart, cantidad });
    res.status(201).json({ message: "Creado" });
});


module.exports = router;