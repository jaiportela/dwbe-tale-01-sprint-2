const express = require("express");
const router = express.Router();
const db = require("../models");

router.get("/", async (req, res) => {
    const models = await db.Modelos.findAll();
    res.json(models);
});

router.post("/", async (req, res) => {
    const referencia = req.body.referencia;
    await db.Modelos.create({ referencia });
    res.status(201).json({ message: "Creado" });
});


module.exports = router;