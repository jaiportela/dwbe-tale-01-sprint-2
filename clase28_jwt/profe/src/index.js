const express = require('express');
const app = express();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const expressJwt = require('express-jwt');
require('./db');
const User = require('./models/user');
const validateUser = require('./schemas/userSchema');
const validateUserLogin = require("./schemas/userLoginSchema");

const MYMEGAPASSWORD =

    app.use(express.json());
app.use(
    expressJwt({
        secret: 'Supercontraseña1234',
        algorithms: ['HS256']
    }).unless({
        path: ['/login', '/register']
    }),
);

app.post('/login', async (req, res) => {
    try {
        const { username, password } = await validateUserLogin.validateAsync(req.body);
        const { email, password: userPassword, isAdmin } = await User.findByUsername(username);
        const validate = bcrypt.compareSync(password, userPassword);
        if (validate) {
            const token = jwt.sign({ email, username, isAdmin }, 'Supercontraseña1234');
            res.json({ token })

        } else {
            res.status(401).json("Unauthorized");
        }
    } catch (error) {
        res.status(401).json(error);
    }

});

app.post('/register', async (req, res) => {
    try {
        const { username, password, email } = await validateUser.validateAsync(req.body);
        const newUser = new User({
            username,
            email,
            password: bcrypt.hashSync(password, 10)
        });
        await newUser.save();
        res.status(201).json(newUser);
    } catch (e) {
        res.status(400).json(e);
    }

});

app.get('/test', async (req, res) => {
    console.log(req);
    res.json(req.user);
});

app.listen(3000, () => { console.log('Escuchando en el puerto 3000') });
