const express = require("express");
const router = express.Router();

const Menu = require("../models/menu");
const validateExistingDish = require("../middlewares/validateExistingDish")

router.get("/", async (req, res) => {
    const allDishes = await Menu.find()
    res.json(allDishes);
});

router.post("/", async (req, res) => {
    const { name, price, category } = req.body;
    const newDish = new Menu({ name, price, category });
    await newDish.save();
    res.json({
        message: "Plato añadido"
    })
});



router.get("/:id", validateExistingDish, async (req, res) => {
    const id = req.params.id;
    const dish = await Menu.findById(id)
    res.json(dish);
});


router.put("/:id", validateExistingDish, async (req, res) => {
    const id = req.params.id;
    const { name, price, category } = req.body
    const editingDish = await Menu.findById(id);
    editingDish.name = name ? name : editingDish.name;
    editingDish.price = price ? price : editingDish.price;
    editingDish.category = category ? category : editingDish.category;
    await editingDish.save();
    res.json(editingDish);
});

router.delete("/:id", validateExistingDish, async (req, res) => {
    const id = req.params.id;
    await Menu.findByIdAndRemove(id);
    res.json({
        message: "Plato eliminado"
    })
});


module.exports = router;