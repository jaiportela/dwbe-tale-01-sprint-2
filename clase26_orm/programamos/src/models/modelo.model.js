module.exports = (sequelize, DataTypes) => {
    const Modelo = sequelize.define("Modelos", {
        referencia: {
            type: DataTypes.STRING
        },
    });

    return Modelo;
}
