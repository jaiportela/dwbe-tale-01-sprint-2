const chai = require('chai');
const app = require('../index');

describe('Test para index y sus funciones', () => {
    it('la funcion saluda deberia devolver: Hola mundo', () => {
        const resultado = app.holamundo();
        chai.assert.equal(resultado, "Hola mundo")
    });

    it('la funcion debe devolver 5 cuando sus parametros son 3 y 2', () => {
        const resultado = app.suma(3, 2);
        chai.assert.equal(resultado, 5);
    });
    it('la funcion debe devolver un numero', () => {
        const resultado = app.suma(3, 2);
        chai.assert.typeOf(resultado, 'number');
    });
});