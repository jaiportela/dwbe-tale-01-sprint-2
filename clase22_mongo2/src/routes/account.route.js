const assert = require("assert")
const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();
const Account = require("../models/account.model")

const { validateEmail, validateAccount } = require("../middlewares/validateEmail")

router.get("/", async (req, res) => {
    const accounts = await Account.find();
    res.json(accounts);
});

router.post("/", validateEmail, async (req, res) => {
    const { name, lastName, email, balance } = req.body;
    const newAccount = await new Account({ name, lastName, email, balance });
    await newAccount.save();
    res.json({
        message: "Cuenta creada"
    });
});

router.put("/modificarSaldo", validateAccount, async (req, res) => {
    const { amount, email } = req.body;
    const existingAccount = await Account.findOne({ email });
    existingAccount.balance += amount;
    await existingAccount.save();
    res.json({ email: existingAccount.email, balance: existingAccount.balance });
});

router.put("/transferencia", async (req, res) => {
    const { amount, emailOrigin, emailDestiny } = req.body;

    const session = await mongoose.startSession();
    session.startTransaction();
    try {
        const doc1 = await Account.findOne({ email: emailOrigin }).session(session);
        assert.ok(doc1);
        const doc2 = await Account.findOne({ email: emailDestiny }).session(session);
        assert.ok(doc2);
        const balance = doc1.balance;
        const difference = balance - amount;
        assert.ok(difference >= 0);
        doc1.balance = difference;
        await doc1.save();
        doc2.balance += amount;
        await doc2.save();
        await session.commitTransaction();
        res.json({
            message: "Transacción exitosa"
        });
    } catch (err) {
        console.log(err);
        res.status(400).json(err);
    }
    session.endSession();
});

module.exports = router;