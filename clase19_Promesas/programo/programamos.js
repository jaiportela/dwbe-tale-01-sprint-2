const fetch = require("node-fetch");

const buscarSeguidores = (nombreUsuario) => {
    fetch(`https://api.github.com/users/${nombreUsuario}/followers`)
        .then(response => response.json())
        .then(jsonRes => {
            let i = 0;
            for (const user of jsonRes) {
                if (i === 5) {
                    break;
                }
                console.log(user);
                i++;
            }
        })
        .catch(err => console.log(err));
}

buscarSeguidores("maosierra");