module.exports = (sequelize, DataTypes) => {
    const Televisor = sequelize.define("televisores", {
        precio: {
            type: DataTypes.DOUBLE
        },
        pantalla: {
            type: DataTypes.STRING
        },
        smart: {
            type: DataTypes.BOOLEAN
        },
        cantidad: {
            type: DataTypes.INTEGER
        }
    });
    return Televisor;
}
