const sequelize = require("./db");
const express = require("express");
const QueryTypes = require("sequelize").QueryTypes;
const app = express();

app.use(express.json());

app.get("/banda", async (req, res) => {
    const bandas = await sequelize.query(`SELECT * FROM bandas_musicales`, {
        type: QueryTypes.SELECT
    })
    res.json(bandas);
});

app.get("/banda/:id", async (req, res) => {
    const id = req.params.id;
    const bandas = await sequelize.query(`
    SELECT bandas_musicales.id as 'id_banda', albumes.nombre_album FROM bandas_musicales
    INNER JOIN albumes
    ON bandas_musicales.id = albumes.banda
    WHERE bandas_musicales.id=?;    
    `, {
        replacements: [id],
        type: QueryTypes.SELECT,
    });
    res.json(bandas);
});

app.post("/", async (req, res) => {
    const { nombre, integrantes, fechaInicio, fechaSeparacion, pais } = req.body;

    await sequelize.query('INSERT INTO bandas_musicales (nombre,integrantes,fecha_inicio,fecha_separacion,pais) VALUES (?,?,?,?,?)', {
        type: QueryTypes.INSERT,
        replacements: [nombre, integrantes, fechaInicio, fechaSeparacion, pais]
    });
    res.status(201).json({ message: "Creado" });
});

app.put("/:id", async (req, res) => {
    const id = req.params.id;
    const { nombre, integrantes, fechaInicio, fechaSeparacion, pais } = req.body;
    await sequelize.query('UPDATE bandas_musicales SET nombre=?,integrantes=?,fecha_inicio=?,fecha_separacion=?,pais=? WHERE id=?; ', {
        type: QueryTypes.UPDATE,
        replacements: [nombre, integrantes, fechaInicio, fechaSeparacion, pais, id]
    });
    res.status(200).json({ message: "Editado" });
});

app.delete("/:id", async (req, res) => {
    const id = req.params.id;
    await sequelize.query('DELETE from bandas_musicales WHERE id=?', {
        type: QueryTypes.DELETE,
        replacements: [id]
    });
    res.status(200).json({ message: "Eliminado" });
});




app.listen(3000);
console.log("Escuchando en el puerto 3000");