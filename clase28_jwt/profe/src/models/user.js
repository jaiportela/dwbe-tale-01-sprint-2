const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const Model = mongoose.model;

const userSchema = new Schema({
    username: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    isAdmin: {
        type: String,
        default: false,
    },
});

userSchema.statics.findByUsername = function (username) {
    return this.findOne({ username });
};

module.exports = Model("User", userSchema);